export interface State {
    token: string,
    user: string,
    project: Record<string, string>,
    scope: Record<string, string>,
    scopeType: string,
    variables: Record<string, Record<string, string>>,
    authInProgress: boolean
    validForm: boolean
}

export interface Getters {
    isAuthenticated: boolean,
    loading: boolean
}

export type GettersDefinition = {
    [P in keyof Getters]: (state: State, getters: Getters) => Getters[P];
}
