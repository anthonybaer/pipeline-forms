import Vue from 'vue'
import VueAuthenticate from '@simbachain/vue-authenticate'
import * as GitLabHelper from '../gitlab'

const baseUrl = GitLabHelper.getHost().replace(/\/?$/, '/')
const authUrl = baseUrl + 'oauth/authorize'
const tokenUrl = baseUrl + 'oauth/token'
const redirectUrl = window.location.origin + window.location.pathname

Vue.use(VueAuthenticate, {
  baseUrl: '',
  providers: {
    gitlab: {
      name: 'GitLab',
      url: tokenUrl,
      clientId: process.env.CLIENT_ID,
      redirectUri: redirectUrl,
      authorizationEndpoint: authUrl,
      oauthType: '2.0',
      pkce: 'S256',
      popupOptions: { width: 600, height: 650 },
      responseParams: {
        clientId: 'client_id',
        redirectUri: 'redirect_uri'
      }
    }
  }
})
