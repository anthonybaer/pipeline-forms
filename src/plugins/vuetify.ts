import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#292961',
        secondary: '#e24329',
        accent: 'fca326'
      },
      dark: {
        primary: '#292961',
        secondary: '#e24329',
        accent: 'fca326'
      }
    }
  }
})
