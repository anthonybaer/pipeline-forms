import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'

import axios from 'axios'
import { State, GettersDefinition } from '../types/store'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'
import * as GitLabHelper from '../gitlab'
import log from 'loglevel'

const ls = new SecureLS({ isCompression: false })

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const state: State = {
  token: '',
  user: '',
  project: {},
  scope: {},
  scopeType: '',
  variables: {},
  authInProgress: false,
  validForm: true
}

const getters: GettersDefinition = {
  isAuthenticated: (state: { token: string }) => state.token !== '',
  loading: (state: { authInProgress: boolean }) => state.authInProgress
}

export default new Vuex.Store({
  state: state,
  getters: getters,
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setProject (state, project) {
      state.project = project
    },
    setAuthInProgress (state, progress) {
      state.authInProgress = progress
    },
    setToken (state, progress) {
      state.token = progress
    },
    setScope (state, scope) {
      state.scope = scope
    },
    setScopeType (state, type) {
      state.scopeType = type
    },
    setVariable (state, { key, type, value }) {
      // if (!!value && value !== '') {
      // } else {
      //   delete state.variables[key]
      // }
      state.variables[key] = { key: key, variable_type: type, value: value }
    },
    setFormValid (state, valid) {
      state.validForm = valid
    },
    clearVariables (state) {
      state.variables = {}
    }
  },
  actions: {
    resetState ({ commit }) {
      commit('setProject', {})
      commit('setScope', {})
      commit('setScopeType', '')
      commit('setVariable', {})
    },
    setUser ({ commit }, user) {
      commit('setUser', user)
    },
    setProject ({ commit }, project) {
      commit('setProject', project)
    },
    setScope ({ commit }, { scope, scopeType }) {
      commit('setScope', scope)
      commit('setScopeType', scopeType)
    },
    setVariable ({ commit }, { key, type, value }) {
      commit('setVariable', { key, type, value })
    },
    clearVariables ({ commit }) {
      commit('clearVariables')
    },
    authenticate ({ commit }, vue: any) {
      commit('setAuthInProgress', true)
      vue.$auth.authenticate('gitlab', {}).then(() => {
        commit('setAuthInProgress', false)
        commit('setToken', vue.$auth.getToken())
        const api = GitLabHelper.getAPI()
        api.Users.current().then((user:any) => {
          log.debug('API user: ' + user)
          commit('setUser', user)
        })
      })
    },
    logout ({ commit }, vue: any) {
      vue.$auth.logout()
      commit('setToken', '')
    },
    setFormValid ({ commit }, valid) {
      commit('setFormValid', valid)
    }
  },
  modules: {
  },
  plugins: [
    createPersistedState({
      paths: ['token', 'user'],
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key)
      }
    })
  ]
})
