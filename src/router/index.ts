import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import PipelineForm from '../views/PipelineForm.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Pipeline Form',
    component: PipelineForm
  },
  {
    path: '/help',
    name: 'Help',
    component: () => import(/* webpackChunkName: "help" */ '../views/Help.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
