import { Gitlab } from '@gitbeaker/browser'
import store from './store/index'

export function getAPI (): InstanceType<typeof Gitlab> {
  return new Gitlab({ oauthToken: store.state.token, host: getHost() })
}

export function getHost (): string {
  return (process.env.GITLAB_URL || 'https://gitlab.com').replace(/\/$/, '')
}
